const Axios = require('axios').default;
const LineReader = require('readline');

// const isPanagram = function(inputstring){
//     var regex = /([a-z])(?!.*\1)/ig;
//     return (inputstring.match(regex) || []).length === 26;
// }

// const callbackPangram = function(sentence){
// 	console.log(sentence);
// }

const isPanagram2 = function (inputstring) {
    return [..."abcdefghijklmnopqrstuvwxyz"].every(x => inputstring.toLowerCase().includes(x));
}

const isPanagramX = function(inputstring) {
	const regex1 = RegExp('([a-z])(?!.*\\1)','ig');
	let array1;
	let setlist = new Set();

	while ((array1 = regex1.exec(inputstring)) !== null) {
		setlist.add(array1[0]);//Set handles duplication		

		if(setlist.size===26){return true;}
	}
	//  console.log(setlist.size);
	return false;
}

//ES5 version to run in MongoDB Stitch serverless
const isPanagram = function(inputstring) {
	const regex1 = RegExp('([a-z])(?!.*\\1)','ig');
	let array1;
	let setlist = [];

	while ((array1 = regex1.exec(inputstring)) !== null) {
		if(!setlist.indexOf(array1[0])>-1){setlist.push(array1[0]);}

		if(setlist.length===26){return true;}
	}
	//  console.log(setlist.size);
	return false;
}

const readPanagramFromStream = async function (downStream, callbackPangram) {
    const readStreamLine = LineReader.createInterface({
        input: downStream,
        // output: process.stdout,
        console: false
    });

    let sentence = '';
    let counter = 1;
    let lineno = 0;
    // let result = [];

    readStreamLine.on('line', function (line) {
        ++lineno;
        // console.log('line=' + lineno);

        const dotcheck = line.split('.');

        if (dotcheck.length == 1) { //everything is part of sentence and no sentence finished yet
            // sentence = [...sentence, ...line];
            sentence = sentence.concat(line);
        }
        else {//>=2
            for (let i = 0; i < dotcheck.length; i++) {
                sentence = sentence.concat(dotcheck[i]);

                if (i < (dotcheck.length - 1)) { //if not last one
                    if (isPanagram(sentence)) {
                        // console.log(counter);
												//WRITE TO MONGODB HERE
												callbackPangram(sentence);
                        // result.push(sentence);
                        counter++;
                        sentence = '';
                    }
                }
                // sentence = ''; //if you want per sentence analysis
            }
        }

    });

    // readStreamLine.on('close', function () {
		// 		console.log(result.length);
		// 		callbackAllPangram(result);
    // });

}

const testPanagramFromStream = async function (inputurl, callbackPangram) {

    // const ts = new Stream.Transform(new Uint8ArrayToStringsTransformer());
    const downStream = await Axios({
        method: 'GET',
        responseType: 'stream',
        url: inputurl,
    })

		readPanagramFromStream(downStream.data, callbackPangram);
		
}

// testPanagramFromStream('http://gutenberg.org/files/47960/47960-0.txt', callbackPangram);

const stitchGetRandomPangram = function(payload, response) {

	// Querying a mongodb service:
	const randompanagram = context.services.get("mongodb-atlas").db("inception").collection("pangram").aggregate([ { $sample: { size: 1 } } ]);
	const result = randompanagram.next();
	
	return result;
};

const stitchStorePangrams = function(gutenbergText,bookId){
  console.log('inside pangram function');

  const atlas = context.services.get('mongodb-atlas');
  
  const line = gutenbergText;
  let sentence = '';
  let counter = 1;
  let lineno = 0;
  let pangramListResult = [];
  
  const returningResult = function(newId){
    console.log(newId);
    pangramListResult.add(newId);
  }
  
  console.log('declare2');
  
  const stitchWriteToPangram = function(sentence){
    console.log('inside write to pangram = '+sentence);
    console.log('inside write to pangram bookId = '+bookId);
    const newPangram = {
        "book_id": BSON.ObjectId(`${bookId}`),
        "pangram": sentence
    };
    atlas.db('inception').collection('pangram').insertOne(newPangram)
        .then(result => returningResult(result.insertedId)) //console.log(`Successfully inserted item with _id: ${result.insertedId}`)
        .catch(err => console.error(`Failed to insert item: ${err}`));
  }
  
  console.log('declare3');
  
  const isPanagramY = function (inputstring) {
    console.log('inside is Panagram');
    return [..."abcdefghijklmnopqrstuvwxyz"].every(x => (inputstring.toLowerCase()).includes(x));
  }
  
const stitchIsPanagram = function(inputstring) {
	const regex1 = RegExp('([a-z])(?!.*\\1)','ig');
	let array1;
	let setlist = [];

	while ((array1 = regex1.exec(inputstring)) !== null) {
		// setlist.add(array1[0]);
		if(!setlist.indexOf(array1[0])>-1){setlist.push(array1[0]);}

		if(setlist.length===26){return true;}
	}
	//  console.log(setlist.size);
	return false;
}
  
  console.log('declare4');
  
  const dotcheck = line.split('.');

  console.log('after split');
  
  if (dotcheck.length == 1) { //everything is part of sentence and no sentence finished yet
      // sentence = [...sentence, ...line];
      sentence = sentence.concat(line);
      console.log('dotcheck1');
  }
  else {//>=2
      console.log('dotcheck.length='+dotcheck.length);
      for (let i = 0; i < dotcheck.length; i++) {
          sentence = sentence.concat(dotcheck[i]);
          // console.log('dotcheck2 = '+sentence);
          if (i < (dotcheck.length - 1)) { //if not last one
              if (isPanagram(sentence)) {
                  console.log(counter);
									//WRITE TO MONGODB HERE
									writeToPangram(sentence);
                  // result.push(sentence);
                  counter++;
                  sentence = '';
              }
          }
          // sentence = ''; //if you want per sentence analysis
      }
      return pangramListResult;
  }
  
};

const stitchStorePangramsFromGutenberg = function(payload, response) {

	const atlas = context.services.get('mongodb-atlas');
	const http = context.services.get('Gutenberg');
	const body = payload.body;
	// console.log("Request body:", body.text());
	
	const bodyjson = JSON.parse(body.text());
	
	
	const gutenbergUrl = bodyjson.gutenbergUrl;
	const cover = bodyjson.cover;
	const title = bodyjson.title;
	const author = bodyjson.author;
	const release = bodyjson.release;
	
	// const gutenbergUrl = "http://gutenberg.org/files/47960/47960-0.txt";
	// const cover = "https://bardolator23.files.wordpress.com/2016/07/embargoed-until-21-30-bst-25-may-2016-kbtc_romeo-and-juliet_garrick-theatre_lily-james-juliet-and-richard-madden-romeo_credit-johan-persson_05910-700x455.jpg";
	// const title = "Tragedy of Romeo and Juliet";
	// const author = "William Shakespeare";
	// const release = "January 13, 2015";
	
	const returnBook = function(gutenbergText, insertedId){
		console.log(insertedId);
		const pangramList = context.functions.execute("pangram", gutenbergText, insertedId);
		
		return pangramList;
	}
	
	const returnGutenberg = function(gutenbergText){
		// console.log(result);
		const newBook = {
			"gutenbergUrl": gutenbergUrl,
			"cover": cover,
			"title": title,
			"author": author,
			"release": release
		};
		atlas.db('inception').collection('book').insertOne(newBook)
			.then(result => returnBook(gutenbergText, result.insertedId)) //console.log(`Successfully inserted item with _id: ${result.insertedId}`)
			.catch(err => console.error(`Failed to insert item: ${err}`));
		
	};
	
	http.get({ url: gutenbergUrl})
		.then(res => returnGutenberg(res.body.text()));
	
};

module.exports = { isPanagram, testPanagramFromStream };