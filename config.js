// const Mongoose = require("mongoose");
const Hapi = require('@hapi/hapi');

// //Init Database
// Mongoose.connect('mongodb+srv://andre:qNQu6l7N7dumAHAj@cluster0-ufejz.mongodb.net/admin?retryWrites=true&w=majority', 
// 		{useNewUrlParser: true, useUnifiedTopology: true});

// const mongoDB = Mongoose.connection;
// mongoDB.on('error', console.error.bind(console, 'connection error:'));
// mongoDB.once('open', function() {
// 	// we're connected!
// 	console.log('MongoDB cluster0-ufejz.mongodb.net connected');
// });

//init server
const init = async () => {

	const server = Hapi.server({
			port: 3000,
			host: 'localhost'
	});

	server.route({
		method: "POST",
		path: "/pangram",
		options: {
				validate: {}
		},
		handler: async (request, h) => {
				try {
						let pangram = new PangramModel(request.payload);
						let result = await pangram.save();
						return h.response(result);
				} catch (error) {
						return h.response(error).code(500);
				}
		}
	});

	await server.start();
	console.log('Server running on %s', server.info.uri);
};

module.exports = {init, mongoDB};