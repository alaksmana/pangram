const isAnagram = require('../anagram');

// 'Abcdefghijklmnopqrstuvwxyz' : true
// 'the quick brown fox jumps over the lazy dog' : true
// 'a quick movement of the enemy will jeopardize five gunboats' : false
// ‘Pack my box with five dozen liquor jugs.’ : true
// 'five boxing wizards jump quickly at it' : true 
// '7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog' : false
// 'The_quick_brown_fox_jumps_over_the_lazy_dog' : true
// '"Five quacking Zephyrs jolt my wax bed."' : true

test('silent and enlist', () => {
    expect(isAnagram('silent', 'enlist')).toBe(true);
});

test('silent and listen', () => {
	expect(isAnagram('silent', 'listen')).toBe(true);
});

test('honor and horor', () => {
	expect(isAnagram('honor', 'horor')).toBe(false);
});