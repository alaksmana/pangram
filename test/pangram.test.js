const {isPanagram, testPanagramFromStream} = require('../pangram');

// 'Abcdefghijklmnopqrstuvwxyz' : true
// 'the quick brown fox jumps over the lazy dog' : true
// 'a quick movement of the enemy will jeopardize five gunboats' : false
// ‘Pack my box with five dozen liquor jugs.’ : true
// 'five boxing wizards jump quickly at it' : true 
// '7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog' : false
// 'The_quick_brown_fox_jumps_over_the_lazy_dog' : true
// '"Five quacking Zephyrs jolt my wax bed."' : true

test('Abcdefghijklmnopqrstuvwxyz', () => {
    expect(isPanagram('Abcdefghijklmnopqrstuvwxyz')).toBe(true);
});

test('the quick brown fox jumps over the lazy dog', () => {
    expect(isPanagram('the quick brown fox jumps over the lazy dog')).toBe(true);
});

test('a quick movement of the enemy will jeopardize five gunboats', () => {
    expect(isPanagram('a quick movement of the enemy will jeopardize five gunboats')).toBe(false);
});

test('Pack my box with five dozen liquor jugs.', () => {
    expect(isPanagram('Pack my box with five dozen liquor jugs.')).toBe(true);
});

test('five boxing wizards jump quickly at it', () => {
    expect(isPanagram('five boxing wizards jump quickly at it')).toBe(false);
});

test('7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog', () => {
    expect(isPanagram('7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog')).toBe(false);
});

test('The_quick_brown_fox_jumps_over_the_lazy_dog', () => {
    expect(isPanagram('The_quick_brown_fox_jumps_over_the_lazy_dog')).toBe(true);
});

test('"Five quacking Zephyrs jolt my wax bed."', () => {
    expect(isPanagram('"Five quacking Zephyrs jolt my wax bed."')).toBe(true);
});

