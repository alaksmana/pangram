const isIsogram = require('../isogram');

// 'Abcdefghijklmnopqrstuvwxyz' : true
// 'the quick brown fox jumps over the lazy dog' : true
// 'a quick movement of the enemy will jeopardize five gunboats' : false
// ‘Pack my box with five dozen liquor jugs.’ : true
// 'five boxing wizards jump quickly at it' : true 
// '7h3 qu1ck brown fox jumps ov3r 7h3 lazy dog' : false
// 'The_quick_brown_fox_jumps_over_the_lazy_dog' : true
// '"Five quacking Zephyrs jolt my wax bed."' : true

test('lumberjacks', () => {
    expect(isIsogram('lumberjacks')).toBe(true);
});

test('backGround', () => {
	expect(isIsogram('backGround')).toBe(true);
});

test('Downstream', () => {
	expect(isIsogram('Downstream')).toBe(true);
});

test('six-year-old', () => {
	expect(isIsogram('six-year-old')).toBe(true);
});

test('isograms', () => {
	expect(isIsogram('isograms')).toBe(false);
});