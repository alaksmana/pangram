

const isIsogram = function (iword) {
	const lowerediword = iword.toLowerCase();
	const minspaceword = lowerediword.replace('-', '');
	const word = minspaceword.replace(' ', '');
	// console.log(word);
	const wordarray = word.split('');
	const wordsorted = wordarray.sort();
	// console.log(wordsorted);
	if (wordsorted.length < 2) {
		// console.log(true);
		return true;
	}
	else if (wordsorted.length == 2) {
		if (wordsorted[0] == wordsorted[1]) {
			// console.log(false);
			return false;
		}
		else {
			// console.log(true);
			return true;
		}
	}
	else {
		for (let i = 0, j = 1; i < wordsorted.length; i++ , j++) {
			// console.log(' = ' + wordsorted[i] + ' ' + wordsorted[j]);
			if (wordsorted[i] == wordsorted[j]) {

				// console.log(false);
				return false;
			}
		}
		// console.log(true);
		return true;
	}
}


// This function is the webhook's request handler.
const stitchScanStreamForIsogram = function(payload, response) {
  //console.log('start');
    const atlas = context.services.get('mongodb-atlas');
    const http = context.services.get('Isogram');
    const body = payload.body;
    // //console.log("Request body:", body.text());
    
    const bodyjson = JSON.parse(body.text());
    
    const wordsUrl = bodyjson.wordsUrl;
    const startFromIndex = bodyjson.startFromIndex;
    const packagesize = bodyjson.packagesize;
    
    const writeManyIsogram = async function(wordList){
      //console.log('before write');
      
      return atlas.db('inception').collection('isogram').insertMany(wordList)
        .then( result => {
          console.log(`Successfully inserted ${result.insertedIds.length} items!`); return true;
        } ) ////console.log(`Successfully inserted item with _id: ${result.insertedId}`)
        .catch(err => {console.log(`Failed to insert item: ${err}`); return false;});
    }
    
    const isIsogram = function (iword) {
	
    	const minspaceword = iword.replace('-', '');
    	const word = minspaceword.replace(' ', '');
    	// console.log(word);
    	const wordarray = word.split('');
    	const wordsorted = wordarray.sort();
    	// console.log(wordsorted);
    	if (wordsorted.length < 2) {
    		// console.log(true);
    		return true;
    	}
    	else if (wordsorted.length == 2) {
    		if (wordsorted[0] == wordsorted[1]) {
    			// console.log(false);
    			return false;
    		}
    		else {
    			// console.log(true);
    			return true;
    		}
    	}
    	else {
    		for (let i = 0, j = 1; i < wordsorted.length; i++ , j++) {
    			// console.log(' = ' + wordsorted[i] + ' ' + wordsorted[j]);
    			if (wordsorted[i] == wordsorted[j]) {
    
    				// console.log(false);
    				return false;
    			}
    		}
    		// console.log(true);
    		return true;
    	}
    }
    
    //console.log('declare processWords');
    const processWords = function(returnWordsText){
      //console.log('inside processWords');
      // let counter = 1;
      let wordList = [];
      const words = returnWordsText.split('\n');
      for(let i = startFromIndex; i < words.length; i++){
        if(!isIsogram(words[i])){
          continue;
        }

        const newIsogram = {
          "wordsUrl": wordsUrl,
          "line": i,
          "word": words[i]
        };
        
        wordList.push(newIsogram);
        if(i%packagesize===0 || i==(words.length-1)){
          writeManyIsogram(wordList);
          wordList = [];
        }
        // counter++;
      }
    };
    
    //console.log('run http');
    //get list of words
    return http.get({ url: wordsUrl})
      .then(res => processWords(res.body.text()));
};


module.exports = isIsogram;