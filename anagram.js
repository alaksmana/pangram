// This function is the webhook's request handler.
// This function is the webhook's request handler.
const stitchGetRandomAnagram = function(payload, response) {
	const randomAnagram = context.services.get("mongodb-atlas").db("inception").collection("anagram").aggregate([{'$group':{'_id':'$key','totalwords':{'$sum':1},'wordList':{'$addToSet':'$word'}}},{'$match':{'totalwords':{'$gt':1}}},{'$sample':{'size':1}}]);
	const result = randomAnagram.next();
	
	return result;
};

const stitchScanStreamForAnagram = function(payload, response) {
  //console.log('start');
    const atlas = context.services.get('mongodb-atlas');
    const http = context.services.get('Anagram');
    const body = payload.body;
    // //console.log("Request body:", body.text());
    
    const bodyjson = JSON.parse(body.text());
    
    const wordsUrl = bodyjson.wordsUrl;
    const startFromIndex = bodyjson.startFromIndex;
    const packagesize = bodyjson.packagesize;
    
    const writeManyAnagram = async function(wordList){
      //console.log('before write');
      
      return atlas.db('inception').collection('anagram').insertMany(wordList)
        .then( result => {
          console.log(`Successfully inserted ${result.insertedIds.length} items!`); return true;
        } ) ////console.log(`Successfully inserted item with _id: ${result.insertedId}`)
        .catch(err => {console.log(`Failed to insert item: ${err}`); return false;});
    }
    
    //console.log('declare processWords');
    const processWords = function(returnWordsText){
      //console.log('inside processWords');
      // let counter = 1;
      let wordList = [];
      const words = returnWordsText.split('\n');
      for(let i = startFromIndex; i < words.length; i++){
        //console.log(i);
        // context.functions.execute("writeAnagram", i, words[i], wordsUrl);
        // const newAnagram = createAnagramObject(i, words[i]);
        const word = words[i];
        const wordarray = word.split('');
        const sortedKey = wordarray.sort();

        const newAnagram = {
          "wordsUrl": wordsUrl,
          "line": i,
          "key": sortedKey.join(''),
          "word": word
        };
        
        wordList.push(newAnagram);
        if(i%packagesize===0 || i==(words.length-1)){
          writeManyAnagram(wordList);
          wordList = [];
        }
        // counter++;
      }
    };
    
    //console.log('run http');
    //get list of words
    return http.get({ url: wordsUrl})
      .then(res => processWords(res.body.text()));
};

const isAnagram = function(word1, word2){
	const word1array = word1.split('');
	const word2array = word2.split('');
	const word1sorted = word1array.sort();
	const word2sorted = word2array.sort();

	if(word1sorted.join('')===word2sorted.join('')){
		return true;
	}
	else{
		return false;
	}
}

module.exports =  isAnagram;