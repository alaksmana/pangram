# MongoDB Stitch
> Serverless functions, REST API and GraphQL endpoints for MongoDB Atlas. 
> The Pangram, Anagram and Isogram are all implemented using MongoDB Stitch.

Every function starts with `stitch` is serverless function implemented in MongoDB Stitch. I apologize for the confusion.

# Postman Collection
Below are the list of API Calls for Pangram, Anagram, and Isogram. You can import the PANGRAM.postman_collection into the Postman app.

# GraphQL

MongoDB has GraphQL feature in beta release. It allows the creation of graphql schema and resolver albeit very limited feature. 

To use it, simply enter the Atlas cluster and in the Rules section create graphql schema. Then get access token and then execute the graphql endpoint. 

You can test this using the Get token then Get graphql:

## GET anonymous access token for MongoDB Stitch
`https://stitch.mongodb.com/api/client/v2.0/app/pangram-kledb/auth/providers/anon-user/login`

## POST Get Anagram with certain key 
`https://stitch.mongodb.com/api/client/v2.0/app/pangram-kledb/graphql`

Body:
```
query {
  anagrams(query:{key: "eilns"}){
    key,
    word
  }
}
```

## POST Get Pangrams with related book
`https://stitch.mongodb.com/api/client/v2.0/app/pangram-kledb/graphql`

Body:
```
query {
  pangrams(limit:10){
    book_id{
      title
      author
      release
      cover
    }
    _id
    pangram
  }
}
```

The relationship between MongoDB collection to another is linked by the ObjectId. Which is not a typical way of NoSQL data normalization. Custom resolver can also be made to do the same thing. 

For example the pangram and novel relationship:

`book`

```
{
  "title": "book",
  "properties": {
    "_id": {
      "bsonType": "objectId"
    },
    "author": {
      "bsonType": "string"
    },
    "cover": {
      "bsonType": "string"
    },
    "gutenbergUrl": {
      "bsonType": "string"
    },
    "release": {
      "bsonType": "string"
    },
    "title": {
      "bsonType": "string"
    }
  }
}
```

`pangram`

```
{
  "title": "pangram",
  "properties": {
    "_id": {
      "bsonType": "objectId"
    },
    "book_id": {
      "bsonType": "objectId"
    },
    "pangram": {
      "bsonType": "string"
    }
  }
}
```

```
{
  "book_id": {
    "ref": "#/stitch/mongodb-atlas/inception/book",
    "foreign_key": "_id",
    "is_list": false
  }
}
```
# PANGRAM

`/lib/plugins/pangram.js`

I use the regex method.

```
const isPanagram = function(inputstring) {
	const regex1 = RegExp('([a-z])(?!.*\\1)','ig');
	let array1;
	let setlist = new Set();

	while ((array1 = regex1.exec(inputstring)) !== null) {
		setlist.add(array1[0]);//Set handles duplication		

		if(setlist.size===26){return true;}
	}
	
	return false;
}
```

Unfortunately it is not running in MongoDB Stitch since Set is not available. There are some troublesome limits.

MongoDB Stitch Constraints:

* Function runtime is limited to 90 seconds.
* Function memory usage is limited to 256MB.
* Functions and uploaded npm modules currently do not support the following ES6+ features:
  New Global Object Types (i.e. WeakMap, Set, WeakSet, Symbol, Proxy)
New Math, Number, String, Array, and Object APIs (e.g. Array.prototype.includes)
* Uploaded npm modules should be transpiled to ES5. Since transpiling to ES5 using Babel is commonplace in the Node.js community, most modules should be available in a transpiled format.
* Functions currently do not support some Node.js built-in modules.

So I used below:

```
const isPanagramES5 = function(inputstring) {
	const regex1 = RegExp('([a-z])(?!.*\\1)','ig');
	let array1;
	let setlist = [];

	while ((array1 = regex1.exec(inputstring)) !== null) {
		if(!setlist.indexOf(array1[0])>-1){setlist.push(array1[0]);}

		if(setlist.length===26){return true;}
	}
	//  console.log(setlist.size);
	return false;
}
```

And with the above constraints, I still have not managed to get it to work propoerly with below functions:

### POST Store Pangram from Gutenberg

`https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pangram-kledb/service/Gutenberg/incoming_webhook/pangramfrom?secret=BWN5xqqQYbDqS93J`

with example Body:

```
{
	"gutenbergUrl":"http://gutenberg.org/files/47960/47960-0.txt",
	"cover":"https://bardolator23.files.wordpress.com/2016/07/embargoed-until-21-30-bst-25-may-2016-kbtc_romeo-and-juliet_garrick-theatre_lily-james-juliet-and-richard-madden-romeo_credit-johan-persson_05910-700x455.jpg",
	"title":"Tragedy of Romeo and Juliet",
	"author":"William Shakespeare",
	"release":"January 13, 2015"
}
```

This API call will make a http GET request to the guntenberUrl and read the entire text. Find Pangram and store it inside MongoDB collection.
The detail is in the `pangram.js` function:
`stitchStorePangramsFromGutenberg` and `stitchStorePangrams`.


### GET random Pangram
The serverless function is implemented using MongoDB Atlas call.
`https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pangram-kledb/service/RandomPangram/incoming_webhook/random?secret=BWN5xqqQYbDqS93J`

```
exports = function(payload, response) {

    // Querying a mongodb service:
    const randompanagram = context.services.get("mongodb-atlas").db("inception").collection("pangram").aggregate([ { $sample: { size: 1 } } ]);
    const result = randompanagram.next();
    
    return result;
};
```

# ANAGRAM

The approach is to first create unique compound index in MongoDB anagram collection to ensure inserted word must be unique:

`key` is sorted characters of the word.
`word` is the word itself.

For example 'silent'. key = eilnst. word = silent. 

Then the function will simply read the text file from http and store inside the MongoDB collection.

### POST Insert Anagrams from Text file (stitchScanStreamForAnagram)

`https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pangram-kledb/service/Anagram/incoming_webhook/scanStreamForAnagram?secret=BWN5xqqQYbDqS93J`

with Body:
```
{
	"wordsUrl":"https://raw.githubusercontent.com/dwyl/english-words/master/words.txt",
	"startFromIndex": 459555,
	"packagesize": 10
}
```
Due to the 90 seconds limitation of the serverless function, I have to separate the scan `startFromIndex` will mention starting from which line no of the text file and `packagesize` limits the size per call to insert the data.

The implementation can be seen at function `stitchScanStreamForAnagram` in the file `anagram.js`.

### GET Random Anagram (stitchGetRandomAnagram)

`https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pangram-kledb/service/Anagram/incoming_webhook/getRandomAnagram?secret=BWN5xqqQYbDqS93J`

The implementation can be seen at function 'stitchGetRandomAnagram' in the file `anagram.js`.

```
// This function is the webhook's request handler.
exports = function(payload, response) {
    const randomAnagram = context.services.get("mongodb-atlas").db("inception").collection("anagram").aggregate([{'$group':{'_id':'$key','totalwords':{'$sum':1},'wordList':{'$addToSet':'$word'}}},{'$match':{'totalwords':{'$gt':1}}},{'$sample':{'size':1}}]);
    const result = randomAnagram.next();
    
    return result;
};
```

The filter can be experimented using MongoDB Compass and MongoDB documentation.

# Isogram

The approach is to remove hyphen and space. Then sort the characters in the string and using pointer check if any duplicates.

```
const isIsogram = function (iword) {
	const lowerediword = iword.toLowerCase();
	const minspaceword = lowerediword.replace('-', '');
	const word = minspaceword.replace(' ', '');
	// console.log(word);
	const wordarray = word.split('');
	const wordsorted = wordarray.sort();
	// console.log(wordsorted);
	if (wordsorted.length < 2) {
		// console.log(true);
		return true;
	}
	else if (wordsorted.length == 2) {
		if (wordsorted[0] == wordsorted[1]) {
			// console.log(false);
			return false;
		}
		else {
			// console.log(true);
			return true;
		}
	}
	else {
		for (let i = 0, j = 1; i < wordsorted.length; i++ , j++) {
			// console.log(' = ' + wordsorted[i] + ' ' + wordsorted[j]);
			if (wordsorted[i] == wordsorted[j]) {

				// console.log(false);
				return false;
			}
		}
		// console.log(true);
		return true;
	}
}
```

The function `stitchScanStreamForIsogram` can be seen at `isogram.js`. It basically reads a text file and write it into the mongoDB isogram collection if the word is isogram.

## POST ScanStreamForIsogram

`https://webhooks.mongodb-stitch.com/api/client/v2.0/app/pangram-kledb/service/Isogram/incoming_webhook/scanStreamForIsogram?secret=BWN5xqqQYbDqS93J`
with Body:
```
{
	"wordsUrl":"https://raw.githubusercontent.com/dwyl/english-words/master/words.txt",
	"startFromIndex": 49971,
	"packagesize": 30000
}
```

The above API will scan the text file and write all the Isogram word into the mongoDB isogram collection.